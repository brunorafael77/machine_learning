import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

class LogisticRegression:
    def __init__(self, H, Y, alpha):
        self.H_ = H
        self.Y_ = Y
        self.alpha_ = alpha
        self.coef_ = self._generate_model(H, Y, alpha);
    
    def _generate_model(self, H, Y, alpha):
        #y = HW + e; This array is the vector parameters W
        d = H.shape[1] + 1
        W = np.matrix([0 for i in range(1, d)]).T
        return self._gradient_descendent_runner(H, Y, W, alpha)
    
    def _gradient_descendent_runner(self, H, Y, W, alpha):
        RSS_NORM = np.linalg.norm(self._RSS_GRADIENT(H, Y, W, alpha))
        #for i in range(0, num_interactions):
        while (RSS_NORM >= 1e-15) :
            W = self._step_gradient(H, Y, W, alpha)
            RSS_NORM = np.linalg.norm(self._RSS_GRADIENT(H, Y, W, alpha))
        
        return W

    def _step_gradient(self, H, Y, W, alpha):   
        new_W = W + self._RSS_GRADIENT(H, Y, W, alpha)
        return new_W
    
    def _RSS_GRADIENT (self, H, Y, W, alpha):
        rate = 2 * alpha
        M1 = rate * H.T

        K = np.dot(H, W)
        M2 = Y-K

        return np.dot(M1, M2)

    def _compute_mse(H, Y, W):
        totalError = 0
        matrix = Y - np.dot(H, W)
        for i in range(0, len(matrix)):
            totalError += sum(matrix[i])

        return totalError / float(len(H))

    def predict(self, X):
        y_pred = np.dot(X, self.coef_)
        return y_pred
       
    def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
        """
        This function prints and plots the confusion matrix.
        Normalization can be applied by setting `normalize=True`.
        """
        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
            print("Normalized confusion matrix")
        else:
            print('Confusion matrix, without normalization')

        print(cm)

        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cm[i, j], fmt),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')

    