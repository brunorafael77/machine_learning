from numpy import *
from linear_regression import *

import matplotlib.pyplot as plt

def run():
    points = genfromtxt('data.csv', delimiter = ',', names = True)
    x = points['hours']
    y = points['score']
    
    polyfit(x, y, 1)
    #hiperparameters
    learning_rate = 0.0001

    #y = mx + b
    initial_m = 0
    initial_b = 0

    num_interations = 1000

    b, m = gradient_descendent_runner(points, initial_b, initial_m, 
    learning_rate, num_interations)
    plt.plot(x, y, '.')
    plt.plot(x, m * x + b, '-')
    plt.show()

if __name__ == '__main__':
    run()